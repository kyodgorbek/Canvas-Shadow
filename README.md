# Canvas-Shadow
Shadow

<div><canvas id="c8" width="200" height = "200" style="border:solid 1px #000000;"></canvas></div>

<script>
var c8 = document.getElementById("c8");
var c8_context = c8.getContext("2d");

function draw_rectangle() {
c8_context.shadowOffsetX = 5;
c8_context.shadowOffsetY = 5;
c8_context.shadowBlur = 10;
c8_context.shadowColor = "DarkGoldenRod";
c8_context.fillStyle = "Gold";
c8_context.fillRect(20, 20, 100, 120);
}
window.onload = draw_rectangle();
</script>
